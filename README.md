Come2China
==========
Copyright mozbugbox <mozbugbox@yahoo.com.au>  
License: GPL 3.0 or later.

This is a FireFox extension.

This extension let you access HTTP page through proxy server based in China.


Usage
=====
Click on the "中" icon to turn on and turn off using China based proxy to
access HTTP pages.

Know Issues
===========
*   HTTPS protocols are not supported by the proxy servers used.
*   Sometime certain proxy server might be offline. If connections cannot
    be made with the addon turned on, try turn the addon OFF and then turn it
    back ON again. The action  will cause the addon to switch to another proxy
    server.

Homepage
========
<https://bitbucket.org/mozbugbox/come2china/>
